module "food-onboarding-alb" {
  source = "git@bitbucket.org:ex-tf-modules/terraform-aws-alb.git"

  load_balancer_type   = "application"
  https_listeners      = []
  target_groups        = []
  aws_region           = "ap-south-1"
  key_name             = "${data.terraform_remote_state.vpc.keypairs}"
  project              = "dev-foodonboarding"
  security_groups      = ["${data.terraform_remote_state.vpc.security-groups["internal"]}"]
  subnets              = ["${data.terraform_remote_state.vpc.private_subnets}"]
  vpc_id               = "${data.terraform_remote_state.vpc.vpc_id}"
  tags = {
    Environment = "${var.environment}"
    BusinessUnit = "${var.businessunit}"
  }
}

module "food-onboarding-autoscaling" {
  source                    = "git@bitbucket.org:ex-tf-modules/terraform-aws-autoscaling.git"
  lc_name                   = "example-lc"
  image_id                  = "${var.ami}"
  instance_type             = "${var.instance_type}"
  security_groups           = ["${var.security_groups}"]
  asg_name                  = "${var.asg}"
  vpc_zone_identifier       = ["${var.subnets}"]
  health_check_type         = "${var.health_check_type}"
  min_size                  = 0
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  tags = {
    Environment = "${var.environment}"
    BusinessUnit = "${var.businessunit}"
  }
}