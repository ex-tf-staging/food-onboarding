data "terraform_remote_state" "base-ami" {
  backend = "s3"

  config {
    bucket     = "base-ami-tfstate"
    key        = "base-ami/base.tfstate"
  }
}

data "terraform_remote_state" "r53" {
    backend = "s3"
    config {
        bucket = "route53-tfstate"
        key = "route53/route53.tfstate"
    }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket     = "vpc-tfstate"
    key        = "vpc/vpc.tfstate"
  }
}
