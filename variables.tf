variable "aws_region" {
  default = "ap-south-1"
}

variable "environment" {
  default = "staging"
}

data "aws_ami" "base-ami" {
  most_recent      = true
  executable_users = ["self"]

  filter {
    name   = "name"
    values = ["base-ami-ubuntu18-Nodejs-*"]
  }

}

terraform {
  backend "s3" {
    bucket     = "example-dev-tfstate"
    key        = "channels/nonprod/onboarding.tfstate"
    region     = "us-west-2"
    dynamodb_table = "terraformnonprod"
  }
}
